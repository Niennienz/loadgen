package main

import (
	"flag"
)

var (
	flagWorkers     int
	flagReqPerSec   int
	flagScheme      string
	flagGraphqlHost string
	flagGraphqlPort int
	flagGraphqlEnd  string
	flagVendor      string
)

func init() {
	flag.IntVar(&flagWorkers, "workers", 100, "Number of concurrent workers.")
	flag.IntVar(&flagReqPerSec, "req", 1, "Number of requests per worker per second.")
	flag.StringVar(&flagScheme, "scheme", "http", "Scheme, http or https.")
	flag.StringVar(&flagGraphqlHost, "host", "localhost", "Host of GraphQL server.")
	flag.IntVar(&flagGraphqlPort, "port", 9000, "port of GraphQL server.")
	flag.StringVar(&flagGraphqlEnd, "end", "graphql", "Endpoint of GraphQL server.")
	flag.StringVar(&flagVendor, "vendor", "pg", "Vendor, default to PostGraphile.")
	flag.Parse()
}
