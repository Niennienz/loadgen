package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/machinebox/graphql"
)

func main() {
	// Concurrency.
	workers := flagWorkers
	reqPerSec := flagReqPerSec
	tickets := make(chan struct{}, workers)
	for i := 0; i < workers; i++ {
		tickets <- struct{}{}
	}
	gap := time.Second / time.Duration(workers) * time.Duration(reqPerSec)
	go func() {
		for range time.Tick(gap) {
			tickets <- struct{}{}
		}
	}()

	go func() {
		time.AfterFunc(time.Second*30, func() {
			os.Exit(1)
		})
	}()

	// GraphQL Client.
	client := graphql.NewClient(fmt.Sprintf("%s://%s:%d/%s", flagScheme, flagGraphqlHost, flagGraphqlPort, flagGraphqlEnd))

	// Hammer.
	for {
		<-tickets
		go func() {
			req := getRequest()
			ctx := context.Background()

			start := time.Now()
			err := client.Run(ctx, req, nil)
			if err != nil {
				log.Println(err)
				tickets <- struct{}{}
				return
			}
			delta := time.Now().Sub(start)
			log.Println(int64(delta) / 10e6)
		}()
	}
}

// Hasura:
// go install loadgen && loadgen -host=192.168.0.181 -port=8080 -end=v1alpha1/graphql -workers=50 -vendor=hr 2> hr_50.txt
// PostGraphile:
// go install loadgen && loadgen -host=192.168.0.181 -port=5000 -end=graphql -workers=50 -vendor=pg 2> pg_50.txt
