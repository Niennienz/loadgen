package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/machinebox/graphql"
)

var (
	requests []*graphql.Request
)

func init() {
	rand.Seed(time.Now().Unix())

	if flagVendor == "hr" {
		req := graphql.NewRequest(`
		query {
			system_items(limit: 100, order_by: id_asc){
				id
			}
		}`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		req = graphql.NewRequest(`
		query {
			users{
				id
				deal_roles_preference
			}
		}
	`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		req = graphql.NewRequest(`
		query {
			deal_versions (limit: 1000, order_by: created_at_asc){
				created_at
				creator_id
			}
		}
	`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		for i := 1; i < 5000; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					deals (where: {id: {_eq : %d}}) {
						id
						people
						deal_type
						dealVersionssBydealId {
							dealVersionRequestssByversionId {
								deal_id
								request_data
								requested_at
								version_id_used
								user_id
								usersByuserId {
									email
									deal_roles_preference
									user_names
								}
							}
							deal_id
						}
					}
				}
				`,
				i))
			requests = append(requests, req)
		}

		for i := 1; i <= 11613; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					files (where: {id: {_eq : %d}}) {
						mime_type
						id
						path
						size
						location
					}
				}
				`,
				i))
			requests = append(requests, req)
		}

		for i := 1; i <= 37469; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					system_items(where: {id: {_eq: %d}}) {
						id
						display_name
						modified_at
						search_vector
						notificationssBysystemItemId {
							id
							info
						}
						systemItemStatusessBysystemItemId {
							item_id_initiator
							state
							id
							systemItemsBysystemItemId {
								aux
								id
								display_name
								systemItemStatusessBysystemItemId {
									id
									updated_at
									visibility
								}
							}
						}
					}
				}
				`,
				i))
			requests = append(requests, req)
		}
	}

	if flagVendor == "pg" {
		req := graphql.NewRequest(`
			query {
				allSystemItems(first: 100, orderBy: ID_ASC) {
					nodes {
						id
					}
				}
			}`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		req = graphql.NewRequest(`
			query {
				allUsers {
					nodes {
						id
						dealRolesPreference
					}
				}
			}
		`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		req = graphql.NewRequest(`
			query {
				allDealVersions(first: 1000, orderBy: CREATED_AT_ASC) {
					nodes {
						createdAt
						creatorId
					}
				}
			}
		`)
		req.Header.Set("Cache-Control", "no-cache")
		requests = append(requests, req)

		for i := 1; i < 5000; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					allDeals(condition: {id: %d}) {
						nodes {
							id
							people
							dealType
							dealVersionsByDealId {
								nodes {
									dealVersionRequestsByVersionId {
										nodes {
											dealId
											requestData
											requestedAt
											versionIdUsed
											userId
											userByUserId {
												email
												dealRolesPreference
												userNames
											}
										dealId
										}
									}
								}
							}
						}
					}
				}
				`,
				i))
			requests = append(requests, req)
		}

		for i := 1; i <= 11613; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					allFiles (condition: {id: %d}) {
						nodes{
						mimeType
						id
						path
						size
						location
						}
					}
				}
				`,
				i))
			requests = append(requests, req)
		}

		for i := 1; i <= 37469; i++ {
			req = graphql.NewRequest(fmt.Sprintf(`
				query {
					allSystemItems(condition: {id: %d}) {
						nodes {
							id
							displayName
							modifiedAt
							searchVector
							notificationsBySystemItemId {
								nodes{
									id
									info
								}
							}
							systemItemStatusesBySystemItemId {
								nodes {
									itemIdInitiator
									state
									id
									systemItemBySystemItemId {
										aux
										id
										displayName
										systemItemStatusesBySystemItemId {
											nodes {
												id
												updatedAt
												visibility
											}
										}
									}
								}
							}
						}
					}
				}
				`,
				i))
			requests = append(requests, req)
		}
	}

}

func getRequest() *graphql.Request {
	return requests[rand.Intn(len(requests))]
}
